import java.util.Scanner;
import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;

/*
Name: Nathan Spelts
Date: April 22nd, 2021
Class: CSE 223
Description: This class creates an indexer which processes a file and indexes each of the words. It stores
each word in a hashmap as a key and puts a linkedlist containing the locations of that word as the value.
The program has numerous methods for getting the number of instances of a word, getting the location of
a word a specific instance, getting the total number of unique words, and printing the hash as a string.
*/

class Indexer {
	// Create hashmap object and call it indexer
	private HashMap <String, LinkedList<Integer>> index = new HashMap<String, LinkedList<Integer>>();
	
	// intialize variables
	boolean processed = false;

	// Constructor
	public Indexer() {
	}

	/* PUBLIC METHODS */

	// Method for processing a file. Opens the file and reads it's contents. While reading it will build an index.
	// If it succeeds it returns true, if it fails it returns false.
	public boolean processFile(String filename) {
		// initialize variables
		Scanner sc;
		String word;
		try {
			sc = new Scanner(new File(filename)); // opens up scanner on given file.
		} catch (Exception e) { // catches file not found error
			return(false);
		}
		// intialize counter
		int count = 1;
		// loops through file
		while (sc.hasNext()) {
			// sets word equal to next word in file
			word = sc.next();
			// checks if the cleaned version of the word is an empty string
			if (!cleanUpWord(word).isEmpty()) {
				// passes the new string and it's location to addReference
				addReference(cleanUpWord(word), count);
				// increments counter so we know the location
				count++;
			}
		}
		// closes file
		sc.close();
		processed = true;
		// success!
		return(true);
	}

	// Method that returns the number of times a given word appears in a file. If the file has not been processed,
	// returns -1. If the word does not appear, returns 0.
	public int numberOfInstances(String word) {
		// checks if the hash has anything in it yet
		if (!processed) {
			// if it doesn't, return -1 since file hasn't been processed
			return -1;
		// checks if the given word is inside the hash
		} else if (index.get(word) == null) {
			// if it isn't return 0
			return 0;
		// this is the scenario that the word is in the hash
		} else {
			// return the size of the linkedlist for this word
			return(index.get(word).size());
		}
	}

	// Method that returns the nth occurence of a given word (first occurence being 0). If the file has not been
	// processed, returns -1. If the word doesn't appear, returns -1. If n is too big or small returns -1.
	public int locationOf(String word, int instanceNum) {
		// checks if the hash has anything in it
		if (!processed) {
			// return -1, file not processed
			return -1;
		// checks if given word is in hash
		} else if (index.get(word) == null) {
			// it's not, return 0
			return 0;
		// word is in hash
		} else {
			// try block in case main goes out of bounds of index
			try {
				// return the location stored in the linked list for that instance number
				return(index.get(word).get(instanceNum));
			} catch (Exception e) {
				// index out of bounds, return -1
				return -1;
			}
		}
	}

	// Method that returns the total number of unique words in the file. Returns -1 if file hasn't been processed.
	public int numberOfWords() {
		// checks if the hash map has anything in it
		if (!processed) {
			// hash empty, retu -1
			return -1;
		}
		// returns size of hashmap
		return(index.size());
	}

	// Method that returns the toString value of the hash table. Returns null if the file hasn't been processed.
	public String toString() {
		// checks if the hashmap is empty
		if (!processed) {
			// return null
			return null;
		}
		// returns hash that's been converted to string
		return(index.toString());
	}

	/* PRIVATE METHODS */

	// Method that removes all characters from a word that aren't letters according to Character.isLetter(). Returns
	// string in uppercase.
	private String cleanUpWord(String word) {
		// declare new string that will be returned as cleaned up version of old string
		String newWord = "";
		// loop through each character of the word
		for (int i = 0; i < word.length(); i++) {
			// if the character is a letter
			if (Character.isLetter(word.charAt(i))) {
				// append the uppercase version to newWord
				newWord += (Character.toUpperCase(word.charAt(i)));
			}
		}
		// return cleaned string
		return(newWord);
	}

	// Method that adds a new location to the end of a linked list. If the word is not yet in the hash, creates it.
	// Does nothing if file hasn't been processed.
	private void addReference(String word, int location) {
		// checks if the word is in the hashmap already
		if (index.get(word) == null) {
			// create new linkedlist
			LinkedList<Integer> newLL = new LinkedList<Integer>();
			// put the new linkedlist in the has with the word as the key
			index.put(word, newLL);
			// add the location of that word to the linkedlist
			newLL.add(location);
		} else {
			// adds location to a word's linkedlist
			index.get(word).add(location);
		}
	}
}
