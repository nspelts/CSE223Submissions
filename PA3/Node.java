import java.io.PrintWriter;

/* 
Name: Nathan Spelts
Class: CSE 223
Date: May 6th, 2021
Description: A class that creates node objects for use in a binary tree.
The nodes have a data variable where a string is stored, an answer variable
which is true if the node is an answer, and false when it's a question. It
also stores the nodes to the left and right of the current node.
*/

class Node {
	/* VARIABLE DECLARATION */	
	String data; // contains string of question/answer
	boolean ans; // if true, node represents an answer
	Node left, right; // for storing left and right children

	// Constructor, takes in question/answer text, and initializes a node object.
	public Node(String n, boolean ans) {
		data = n; // assigned passed string to data field of node
		left = right = null; // initializes node's children to null
	}

	// method for printing in NLR form to file
	public void print(PrintWriter pw) {
		if (ans) pw.println("A:"); // if the current node is an answer, print "A:" to file
		else pw.println("Q:"); // else print "Q:" to file
		pw.println(data); // print text stored in node to file
		if (left != null) left.print(pw); // if the node has a left child, return it for recursion
		if (right != null) right.print(pw); // if the node has a right child, return it for recursion
	}
}
