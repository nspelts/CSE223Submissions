import java.util.Scanner;
import java.io.PrintWriter;
import java.io.File;

/*
Name: Nathan Spelts
Class: CSE223
Date: May 6th, 2021
Description: This class contains the main program for 20 questions where the loop is and the game is played.
It also contains methods for reading a file, and building a binary tree of questions and answers from it's
contents. The program takes in a command line for a database txt file, but if one is not provided 20Q.txt is
used as a default.
*/

class PA3 {

	public static void main(String[] args) {
		// loop so game can be played consecutively, ctrl+c to exit
		while (true) {
			// intro text for the user
			System.out.println("\n\nHello, how about a game of 20 questions?");
			System.out.println("\nThink of an object, and I'll try to guess what it is.");
	
			// intialize scanner
			Scanner answer = new Scanner(System.in);
			// initialize response which is used to store user input and also 
			// lastResponse, which stores the response before it changes
			String response = "", lastResponse;
			// creates new PA3 object
			PA3 test = new PA3();
			// initializes root variable, which keeps track of the root of the whole tree as well as
			// previous node, which keeps track of node that points to the current node
			Node root, previous = null;
			// if there's no command line argument it uses 20Q.txt otherwise it uses the command line
			// argument
			if (args.length == 0) root = test.processFile("20Q.txt");
			else root = test.processFile(args[0]);
			// intializes new node and copies root to it (this is the current node variable)
			Node node = root;
			// loop for gameplay
			while (true) {
				// if the node is an answer to a question
				if (node.ans) {
					// records last response before getting new one
					lastResponse = response;
					
					// makes guess and asks user if we were right
					System.out.println("Is it a " + node.data + "? (y/n)");
					response = answer.nextLine();

					// if the user said "y" or something starting with it, indicating we were right.
					if (response.substring(0,1).equals("y")) {
						System.out.println("\nHooray! Thanks for playing. :)\n");
					} else {
						// in this case we were wrong, so we ask what the user was thinking of
						System.out.println("What were you thinking of?");
						response = answer.nextLine();

						// now we ask the user for a new question for the future
						System.out.println("What's a question where yes means a " + response + " and no means a " + node.data + "?");
						String question = answer.nextLine();
						// the guess we made is set as the right child, now if the new question is
						// asked and the answer is no, we'll make the same guess again
						Node right = node;
						// the current node is now set to the new question provided by the user
						node = new Node(question, false);
						// the left child is set to what the user was thinking of, now when the
						// new question is asked and the user says yes, this will be the guess
						node.left = new Node(response, true);
						// sets new answer to an answer
						node.left.ans = true;
						// assigns new right node
						node.right = right;
	
						// this sets the previous node to point to our new question
						// the if/else just determines if the new question is to the
						// right or left of the previous node.
						if (lastResponse.substring(0,1).equals("y")) previous.left = node;
						else previous.right = node;
					}
					// break out of gameplay loop
					break;
				}
				// in this case, the node we're on is a question, this print statement asks it
				System.out.println(node.data + "? (y/n)");
				// this loops until input is valid
				while (true) {
					// records input from user
					response = answer.nextLine();
					// records previous node
					previous = node;
					if (response.substring(0,1).equals("y")) {
						// if answer to question is yes, moves left
						node = node.left;
						break;
					} else if (response.substring(0,1).equals("n")) {
						// if answer to question is no, moves right
						node = node.right;
						break;
					}
				}
			}
			// passes root of tree to outputToFile method so it can be recorded in a text file
			// if/else is to determine if the file is from command line or default
			if (args.length == 0) test.outputToFile(root, "20Q.txt");
			else test.outputToFile(root, args[0]);
		}
	}

	// method for opening up the scanner, passing it to ingest, and closing the file afterward
	public Node processFile(String filename) {
		// initialize variables
		Scanner sc;
		String line;
		
		// try/catch in case file cannot be opened
		try {
			// opens the scanner on provided file
			sc = new Scanner(new File(filename));
		} catch (Exception e) {
			// file couldn't be opened, return null
			return null;
		}

		// call ingest on scanner and return root to this variable
		Node root = ingest(sc);
		// close file
		sc.close();
		// return root
		return(root);
	}
	
	// this method is for recursively looping through a database file and
	// building a binary tree from it's contents
	private Node ingest(Scanner sc) {
		// reads line from file into line variable
		String line = sc.nextLine();
		// intializes counter (this is for catching root node)
		int count = 0;
		// initialize root node
		Node root;
		// initialize text variable
		String text;
		// checks if line has a Q on it
		if (line.substring(0, 1).equals("Q")) {
			// goes to line after the Q
			text = sc.nextLine();
			// creates new node with the text from this line as it's data, flags as question
			Node node = new Node(text, false);
			// if the counter is at 0, the node is the root node so we save it
			if (count == 0) root = node;
			
			// now we start looping recursively to the left until we hit a leaf node
			node.left = ingest(sc);
			// once we hit a leaf node we start looping recursively to the right until we hit another leaf node
			node.right = ingest(sc);
			// increment count, kind of useless once we have root
			count++;
			// return node
			return(node);
		// triggers when if line has A on it (it's also a leaf node)
		} else {
			// goes to line after A
			text = sc.nextLine();
			// creates new node with text from line, flags as answer
			Node node = new Node(text, true);
			// sets answer to true
			node.ans = true;
			// sets leaf node's left and right children to null
			node.left = node.right = null;
			// return node
			return(node);
		}
	}
	
	// method for writing binary tree from memory to text file
	private void outputToFile(Node root, String file) {
		// declares new print writer
		PrintWriter pw = null;
		// initializes print writer inside try block to confirm it can be opened
		try {
			pw = new PrintWriter(new File(file));
		} catch (Exception e) {
		}
		// passes print writer to node print method
		root.print(pw);
		// closes print writer
		pw.close();
	}
}
