
/*
 * Names:
 * Nathan Spelts
 * Thomas Thanem
 * Logan McCartney
 * 
 * */

public class Cell {
	
	Integer index, westCoord, eastCoord, northCoord, southCoord, owner;
	Boolean north, south, east, west;
	public Cell() {
		north = south = east = west = false;
		westCoord = eastCoord = northCoord = southCoord = owner = 0;
	}
	
	public String toString() {
		return("Index: " + index + " North: " + north + " South: " + south + " East: " + east + " West: " + west + "\nnorthCoord: " + northCoord + " southCoord: " + southCoord + " eastCoord: " + eastCoord + " westCoord: " + westCoord + "\n");
	}
}
