import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;


/*
 * Names:
 * Nathan Spelts
 * Logan McCartney
 * Thomas Thanem
 * 
 * Date:
 * May 21st, 2021
 * 
 * Class:
 * CSE 223
 * 
 * Description:
 * This class controls the graphical user interface layout, it controls mouse listeners, button listeners, text fields etc.
 * It also contains much of the logic for determining where a mouse click is. It contains logic for cell completion, name storage,
 * score keeping etc. As well as logic for when a bad move is made or for which player's turn it is.
 * */

@SuppressWarnings("serial")
public class Dots extends JFrame {

	// variable initialization
	private JPanel contentPane;
	private JTextField player1field;
	private JTextField player2field;
	private JTextField whoseTurn;
	boolean player1turn = true; // used for determining whose turn it is
	boolean completedBox = false, completedAboveBox = false, completedBelowBox = false, completedLeftBox = false, completedRightBox = false; // used for keeping track of whether a cell is completed
	boolean badMove = true; // keeps track of when a player tries to make a forbidden move, gives them another turn
	Cell aboveCell, belowCell, leftCell, rightCell; // used for tracking cells around the current cell
	// for tracking scores
	static int player1ScoreCount = 0; 
	static int player2ScoreCount = 0;

	private JTextField player1Score;
	private JTextField player2Score;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Dots frame = new Dots();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Dots() {
		setBackground(Color.DARK_GRAY);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1155, 818);
		contentPane = new JPanel();
		contentPane.setForeground(Color.WHITE);
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		MyPanel playGround = new MyPanel();
		playGround.setBounds(376, 29, 740, 740);
		playGround.setForeground(Color.LIGHT_GRAY);
		playGround.setBackground(Color.GRAY);
		contentPane.add(playGround);
		
		// listener that is activated when the mouse moves within the playground
		playGround.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				// variable that tracks the current cell the mouse is moving in
				Cell currentCell;
				// booleans for tracking which border the mouse is closest to
				boolean north = false, south = false, east = false, west = false;
				// this is where we determine the coordinates of the corners of the current cell, as well as the index
				currentCell = playGround.determineCurrentCell(e.getX(), e.getY(), true);
				// if we're in the margin, currentCell will be null.
				if (currentCell != null) {
					
					// here we calculate the distances from each edge of our currentCell
					int westDist = e.getX() - currentCell.westCoord;
					int eastDist = currentCell.eastCoord - e.getX();
					int northDist = e.getY() - currentCell.northCoord;
					int southDist = currentCell.southCoord - e.getY();
					
					// this logic determines if we're closer to the west edge or east edge
					if (westDist < eastDist) west = true;
					else east = true;
					
					// this logic determines if we're closer to the north edge or south edge
					if (northDist < southDist) north = true;
					else south = true;
					
					if (west && north) {
						// since we're close to north and west
						// we see which one we're closer to
						if (westDist < northDist) north = false;
						else west = false;
					} else if (west && south) {
						// since we're close to south and west
						// we see which one we're closer to
						if (westDist < southDist) south = false;
						else west = false;
					} else if (east && north) {
						// since we're close to north and east
						// we see which one we're closer to
						if (eastDist < northDist) north = false;
						else east = false;
					} else if (east && south) {
						// since we're close to east and south
						// we see which one we're closer to
						if (eastDist < southDist) south = false;
						else east = false;
					}
					// now we tell MyPanel to highlight the line we're closest to for a cool visual effect!
					if (north) playGround.highlightLine(currentCell.westCoord, currentCell.northCoord, currentCell.eastCoord, currentCell.northCoord);
					else if (south) playGround.highlightLine(currentCell.westCoord, currentCell.southCoord, currentCell.eastCoord, currentCell.southCoord);
					else if (east) playGround.highlightLine(currentCell.eastCoord, currentCell.northCoord, currentCell.eastCoord, currentCell.southCoord);
					else if (west) playGround.highlightLine(currentCell.westCoord, currentCell.northCoord, currentCell.westCoord, currentCell.southCoord);
					// we tell MyPanel to repaint with it's new data
					repaint();
				}
			}
		});
		
		// listener for tracking where the mouse is clicked
		playGround.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// this if statement prevents altering cells until the game has started
				if (playGround.initialized) {
					// this if statement is to catch the edge case where the margin is clicked and weird behavior occurs
					if (!(e.getX() >= playGround.getWidth() - playGround.margin - 6)) {
						// these variables keep track of the edges of the currentCell
						boolean north = false, south = false, east = false, west = false;
						// this variable calls a method in MyPanel to determine the index and corner coordinates for our cell
						Cell currentCell = playGround.determineCurrentCell(e.getX(), e.getY(), false);
						// currentCell is null if the user clicks in the margin.
						if (currentCell != null) {
							
							// this is logic to determine the distances from each edge
							int westDist = e.getX() - currentCell.westCoord;
							int eastDist = currentCell.eastCoord - e.getX();
							int northDist = e.getY() - currentCell.northCoord;
							int southDist = currentCell.southCoord - e.getY();
							
							//System.out.println(currentCell);
							
							// here we see if we're closer to the east or west
							if (westDist < eastDist) west = true;
							else east = true;
							
							// here we see if we're farther south or north
							if (northDist < southDist) north = true;
							else south = true;
							
							if (west && north) {
								// since we're close to north and west
								// we see which one we're closer to
								if (westDist < northDist) north = false;
								else west = false;
							} else if (west && south) {
								// since we're close to south and west
								// we see which one we're closer to
								if (westDist < southDist) south = false;
								else west = false;
							} else if (east && north) {
								// since we're close to north and east
								// we see which one we're closer to
								if (eastDist < northDist) north = false;
								else east = false;
							} else if (east && south) {
								// since we're close to east and south
								// we see which one we're closer to
								if (eastDist < southDist) south = false;
								else east = false;
							}
							
							// if we determines we're closest to the north we go here
							if (north) {
								// this if statement is triggered if the north has already been marked true
								if (playGround.cellArray.get(currentCell.index).north) {
									// this means that the player made a bad move, they're told and given another turn
									badMove = true;
									if (player1turn) whoseTurn.setText("Bad move. " + "It's still " + player1field.getText() + "'s turn.");
									else whoseTurn.setText("Bad move. " + "It's still " + player2field.getText() + "'s turn.");
								} else {
									// badMove is set to false, so the player doesn't get another turn
									badMove = false;
									
									// the north is set to true for the currentCell, and it's updated with any information already present at that index
									// within the linked list.
									currentCell.north = true;
									currentCell.south = playGround.cellArray.get(currentCell.index).south;
									currentCell.east = playGround.cellArray.get(currentCell.index).east;
									currentCell.west = playGround.cellArray.get(currentCell.index).west;
									
									// now we add the currentCell to the link list (which tells the paint method to draw it
									playGround.cellArray.set(currentCell.index, currentCell);
									
									// this if statement prevents leaving the bounds of the index
									if (currentCell.index - playGround.cells >= 0) {
										// here we retrieve the cell above so we can mark it's south variable as true
										aboveCell = playGround.cellArray.get(currentCell.index - playGround.cells);
										aboveCell.south = true;
										
										// this is where we check if the above cell was completed with our move
										if (aboveCell.north && aboveCell.east && aboveCell.west) {
											// if it was, we mark this variable and deal with it later
											completedAboveBox = true;
										}
									}
									// checks if box is completed and sets flag if it is
									if (currentCell.south && currentCell.east && currentCell.west) completedBox = true;
								}
							// if we're closest to south we go here
							} else if (south) {
								// checks the cellArray to see if the south boolean is already true
								if (playGround.cellArray.get(currentCell.index).south) {
									// marks bad move so player gets another turn
									badMove = true;
									// informs the player they made a bad move
									if (player1turn) whoseTurn.setText("Bad move. " + "It's still " + player1field.getText() + "'s turn.");
									else whoseTurn.setText("Bad move. " + "It's still " + player2field.getText() + "'s turn.");
								} else {
									// turns bad move off so the player doesn't get another turn
									badMove = false;
									
									// sets current cell south to true, and updates it's other boolean flags from LinkedList
									currentCell.south = true;
									currentCell.north = playGround.cellArray.get(currentCell.index).north;
									currentCell.east = playGround.cellArray.get(currentCell.index).east;
									currentCell.west = playGround.cellArray.get(currentCell.index).west;
									
									// adds currentCell to the LinkedList so it can be drawn
									playGround.cellArray.set(currentCell.index, currentCell);
									
									// if statement catches edge case to prevent out of bounds exception
									if (!(playGround.cells + currentCell.index - 1 >= playGround.cells * playGround.cells)) {
										// grabs the location of cell below current cell
										belowCell = playGround.cellArray.get(playGround.cells + currentCell.index);
										
										// sets the north field of the field below to true
										belowCell.north = true;
										// checks to see if the box below became complete
										if (belowCell.south && belowCell.east && belowCell.west) {
											completedBelowBox = true;
										}
									}
									// checks to see if the currentCell was completed
									if (currentCell.north && currentCell.east && currentCell.west) completedBox = true;
								}
							// if we're closest to east, go here
							} else if (east) {
								// checks to see if the east flag was already set
								if (playGround.cellArray.get(currentCell.index).east) {
									// if it was, give player another turn and inform them they made an oopsie
									badMove = true;
									if (player1turn) whoseTurn.setText("Bad move. " + "It's still " + player1field.getText() + "'s turn.");
									else whoseTurn.setText("Bad move. " + "It's still " + player2field.getText() + "'s turn.");
								} else {
									// sets badMove flag back so player doesn't get another turn
									badMove = false;
									// sets current cell east to true and updates it's other flags from data in linkedlist
									currentCell.east = true;
									currentCell.south = playGround.cellArray.get(currentCell.index).south;
									currentCell.north = playGround.cellArray.get(currentCell.index).north;
									currentCell.west = playGround.cellArray.get(currentCell.index).west;
									// add current cell to linked list to be drawn
									playGround.cellArray.set(currentCell.index, currentCell);
									
									// if statement prevents out of bounds exception
									if (currentCell.index % playGround.cells != 0) {
										// sets rightCell equal to the cell to the right of the current cell
										rightCell = playGround.cellArray.get(currentCell.index + 1);
										
										// sets right cell's west field to true
										rightCell.west = true;
										// checks if the rightCell was completed
										if (rightCell.north && rightCell.east && rightCell.south) {
											completedRightBox = true;
										}
									}
									// checks if current cell was completed
									if (currentCell.north && currentCell.south && currentCell.west) completedBox = true;
								}
							// if the closest edge was west, go here
							} else if (west) {
								// checks if west flag was already set
								if (playGround.cellArray.get(currentCell.index).west) {
									// informs player they made a bad move and gives them another turn
									badMove = true;
									if (player1turn) whoseTurn.setText("Bad move. " + "It's still " + player1field.getText() + "'s turn.");
									else whoseTurn.setText("Bad move. " + "It's still " + player2field.getText() + "'s turn.");
								} else {
									// sets badMove flag back so player doesn't get another turn
									badMove = false;
									// sets west flag true, and updates other flags from LL
									currentCell.west = true;
									currentCell.south = playGround.cellArray.get(currentCell.index).south;
									currentCell.east = playGround.cellArray.get(currentCell.index).east;
									currentCell.north = playGround.cellArray.get(currentCell.index).north;
									// puts current cell into LL
									playGround.cellArray.set(currentCell.index, currentCell);
									// if statement makes sure we don't go out of bounds
									if ((currentCell.index % playGround.cells) - 1 != 0) {
										// sets left cell equal to cell to the left of current cell
										leftCell = playGround.cellArray.get(currentCell.index - 1);
										// sets east flag of left cell to true
										leftCell.east = true;
										// checks to see if we completed the cell to the left
										if (leftCell.north && leftCell.west && leftCell.south) {
											completedLeftBox = true;
										}
									}
									// checks to see if we completed the current box
									if (currentCell.north && currentCell.south && currentCell.east) completedBox = true;
								}
							}
							// skips this logic if badMove is true
							if (!badMove) {
								// if statement triggered if any box is completed
								if ((completedBox) || (completedAboveBox) || (completedBelowBox) || (completedLeftBox) || (completedRightBox)) {
									// if current cell was completed
									if (completedBox) {
										// if player1 completed it
										if (player1turn) {
											// sets owner flag to one
											currentCell.owner = 1;
											// tell markCell method so it can put initials on cell
 											playGround.markCell(currentCell, player1field.getText(), player2field.getText());
 											// add 1 to player 1's score
											player1ScoreCount++;
											// update player 1's score text display
											player1Score.setText(player1field.getText() + "'s Score: " + player1ScoreCount);
										// if player 2 completed it
										} else {
											// sets owner flag to 2
											currentCell.owner = 2;
											// increment player 2's score
											player2ScoreCount++;
											// tell markCell method to write initials in cell
											playGround.markCell(currentCell, player1field.getText(), player2field.getText());
											// update score text
											player2Score.setText(player2field.getText() + "'s Score: " + player2ScoreCount);
										}
										// turn off completed box flag
										completedBox = false;
									}
									// if cell above was completed
									if (completedAboveBox) {
										// if player 1 completed it
										if (player1turn) {
											// set owner flag to 1
											aboveCell.owner = 1;
											// tell markCell to put initials on cell
											playGround.markCell(aboveCell, player1field.getText(), player2field.getText());
											// increment player 1 score
											player1ScoreCount++;
											// update text field
											player1Score.setText(player1field.getText() + "'s Score: " + player1ScoreCount);
										// if player 2 completed it
										} else {
											// sets owner flag to 2
											aboveCell.owner = 2;
											// increments player 2's score
											player2ScoreCount++;
											// tells markCell method to add initials to cell
											playGround.markCell(aboveCell, player1field.getText(), player2field.getText());
											// updates text field with score
											player2Score.setText(player2field.getText() + "'s Score: " + player2ScoreCount);
										}
										// turns off completed above box flag
										completedAboveBox = false;
									}
									// if cell below was completed
									if (completedBelowBox) {
										// if player 1 completed it
										if (player1turn) {
											// set owner flag to 1
											belowCell.owner = 1;
											// tell mark cell to put initials on cell
											playGround.markCell(belowCell, player1field.getText(), player2field.getText());
											// increment player 1 score
											player1ScoreCount++;
											// update score text field
											player1Score.setText(player1field.getText() + "'s Score: " + player1ScoreCount);
										// if player 2 completed it
										} else {
											// set owner flag to 2
											belowCell.owner = 2;
											// increment player 2 score
											player2ScoreCount++;
											// tell mark cell to put initials on the cell
											playGround.markCell(belowCell, player1field.getText(), player2field.getText());
											// update score text field
											player2Score.setText(player2field.getText() + "'s Score: " + player2ScoreCount);
										}
										// reset completedBelowBox flag
										completedBelowBox = false;
									}
									// if cell to the left was completed
									if (completedLeftBox) {
										// if player 1 completed it
										if (player1turn) {
											// set owner flag to 1
											leftCell.owner = 1;
											// tells mark cell to put initials in box
											playGround.markCell(leftCell, player1field.getText(), player2field.getText());
											// increment player 1's score
											player1ScoreCount++;
											// update score text field
											player1Score.setText(player1field.getText() + "'s Score: " + player1ScoreCount);
										// if player 2 completed it
										} else {
											// set owner flag to 2
											leftCell.owner = 2;
											// increment player 2's score
											player2ScoreCount++;
											// tells mark cell to put initials in box
											playGround.markCell(leftCell, player1field.getText(), player2field.getText());
											// updates score text
											player2Score.setText(player2field.getText() + "'s Score: " + player2ScoreCount);
										}
										// reset completed left box flag
										completedLeftBox = false;
									}
									// if cell to the right is completed
									if (completedRightBox) {
										// if player 1 completed it
										if (player1turn) {
											// sets owner flag to 1
											rightCell.owner = 1;
											// tells mark cell to put initials in box
											playGround.markCell(rightCell, player1field.getText(), player2field.getText());
											// increment player 1's score
											player1ScoreCount++;
											// updates score text
											player1Score.setText(player1field.getText() + "'s Score: " + player1ScoreCount);
										} else {
											// sets owner flag to 2
											rightCell.owner = 2;
											// increment player 2 flag
											player2ScoreCount++;
											// tells mark cell to put initials in box
											playGround.markCell(rightCell, player1field.getText(), player2field.getText());
											// updates score text
											player2Score.setText(player2field.getText() + "'s Score: " + player2ScoreCount);
										}
										// resets completedrightbox flag
										completedRightBox = false;
									}
								// switches turns
								} else {
									if (player1turn) {
										player1turn = false;
									} else {
										player1turn = true;
									}
								}
							}
						}
						// changes turn message in text field
						if (!badMove) {
							if (player1turn) {
								whoseTurn.setText("It's " + player1field.getText() + "'s turn.");
							} else {
								whoseTurn.setText("It's " + player2field.getText() + "'s turn.");
							}
						}
						// repaints everything that was changed
						repaint();

						if (player1ScoreCount + player2ScoreCount == playGround.cells * playGround.cells) {
							if (player1ScoreCount > player2ScoreCount) {
								whoseTurn.setText(player1field.getText() + " won!");
							} else {
								whoseTurn.setText(player2field.getText() + " won!");
							}
						}
					}
				}
			}
		});
		// playGround.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		
		JTextPane cellCount = new JTextPane();
		cellCount.setBackground(Color.DARK_GRAY);
		cellCount.setForeground(Color.WHITE);
		cellCount.setFont(new Font("Dialog", Font.PLAIN, 12));
		cellCount.setEditable(false);
		cellCount.setText(Integer.toString(playGround.cells + 1));
		cellCount.setBounds(469, 2, 21, 18);
		contentPane.add(cellCount);
		
		JButton zoomIn = new JButton("+");
		zoomIn.setBackground(Color.GRAY);
		zoomIn.setForeground(Color.LIGHT_GRAY);
		zoomIn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// flag prevents zooming once game has begun
				if (!playGround.initialized) {
					// adds one cell
					playGround.cells += 1;
					// changes cell text
					cellCount.setText(Integer.toString(playGround.cells + 1));
					// repaints grid
					repaint();
				}
			}
		});
		zoomIn.setBounds(12, 29, 117, 25);
		contentPane.add(zoomIn);
		
		JButton zoomOut = new JButton("-");
		zoomOut.setForeground(Color.LIGHT_GRAY);
		zoomOut.setBackground(Color.GRAY);
		zoomOut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// flag prevents zooming once game has begun
				if (!playGround.initialized) {
					// removes one cell
					playGround.cells -= 1;
					// changes cell count text
					cellCount.setText(Integer.toString(playGround.cells + 1));
					// repaints grid
					repaint();
				}
			}
		});
		zoomOut.setBounds(174, 29, 117, 25);
		contentPane.add(zoomOut);
		
		JLabel lblCellCount = new JLabel("Cell Count:");
		lblCellCount.setForeground(Color.WHITE);
		lblCellCount.setBounds(382, 0, 93, 25);
		contentPane.add(lblCellCount);
		
		player1field = new JTextField();
		player1field.setText("1st Player");
		player1field.setForeground(Color.BLACK);
		player1field.setBackground(Color.GRAY);
		player1field.setBounds(15, 106, 114, 19);
		contentPane.add(player1field);
		player1field.setColumns(10);
		
		player2field = new JTextField();
		player2field.setText("2nd Player");
		player2field.setForeground(Color.BLACK);
		player2field.setBackground(Color.GRAY);
		player2field.setBounds(174, 106, 114, 19);
		contentPane.add(player2field);
		player2field.setColumns(10);
		
		JLabel lblPlayer = new JLabel("1st Player");
		lblPlayer.setForeground(Color.LIGHT_GRAY);
		lblPlayer.setBounds(15, 79, 70, 15);
		contentPane.add(lblPlayer);
		
		JLabel lblPlayer_1 = new JLabel("2nd Player");
		lblPlayer_1.setForeground(Color.LIGHT_GRAY);
		lblPlayer_1.setBounds(174, 79, 70, 15);
		contentPane.add(lblPlayer_1);
		
		JButton start = new JButton("Start");
		start.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if (player1field.getText().substring(0, 1).equals(player2field.getText().substring(0, 1))) {
					whoseTurn.setText("The names must have different initials.");
				} else {
				
					// initializes LinkedList when start is pressed
					if (!playGround.initialized) playGround.initializeLL();
					// flag prevents zooming and name changes
					playGround.initialized = true;
					// changes text to restart
					start.setText("restart");
					// prevents name editing
					player1field.setEditable(false);
					player2field.setEditable(false);
					
					// if restart flag is set
					if(playGround.restart) {
						// call restartGame method
						playGround.restartGame();
					}
					else {
						// otherwise set restart to true
						playGround.restart=true;
					}
				
					// sets beginning score
					player1Score.setText(player1field.getText() + "'s Score: 0");
					player2Score.setText(player2field.getText() + "'s Score: 0");
					
					// player 1 goes first message
					whoseTurn.setText(player1field.getText() + " goes first.");
				}
			}
		});
		start.setBackground(Color.GRAY);
		start.setBounds(95, 177, 117, 25);
		contentPane.add(start);
		
		whoseTurn = new JTextField();
		whoseTurn.setBorder(null);
		whoseTurn.setForeground(Color.LIGHT_GRAY);
		whoseTurn.setBackground(Color.DARK_GRAY);
		whoseTurn.setEditable(false);
		whoseTurn.setBounds(12, 243, 279, 35);
		contentPane.add(whoseTurn);
		whoseTurn.setColumns(10);
		
		player1Score = new JTextField();
		player1Score.setText("Player 1's Score: ");
		player1Score.setBorder(null);
		player1Score.setEditable(false);
		player1Score.setForeground(Color.LIGHT_GRAY);
		player1Score.setBackground(Color.DARK_GRAY);
		player1Score.setBounds(15, 320, 137, 19);
		contentPane.add(player1Score);
		player1Score.setColumns(10);
		
		player2Score = new JTextField();
		player2Score.setBorder(null);
		player2Score.setText("Player 2's Score:");
		player2Score.setEditable(false);
		player2Score.setForeground(Color.LIGHT_GRAY);
		player2Score.setBackground(Color.DARK_GRAY);
		player2Score.setBounds(15, 368, 137, 19);
		contentPane.add(player2Score);
		player2Score.setColumns(10);
	}
}
