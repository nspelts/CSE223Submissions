import java.awt.Color;
import java.awt.Graphics;
import java.util.LinkedList;

import javax.swing.JPanel;

/*
 *Names:
 *Nathan Spelts
 *Thomas Thanem
 *Logan McCartney
 */

@SuppressWarnings("serial")//hide our mistakes with a supress warning
public class MyPanel extends JPanel {
	//initialization of basic variables and linked lists.
	int cells = 8, margin = 20;
	private int x1 = 0, y1 = 0, x2 = 0, y2 = 0;//these are used with highlightline to be coordinate variables
	boolean initialized = false;
	boolean restart = false;
	String player1 = "1st Player", player2 = "2nd Player";
	LinkedList<Cell> cellArray = new LinkedList<Cell>();
	LinkedList<Cell> markedCells = new LinkedList<Cell>();
	
	//paint function :)
	public void paint(Graphics g) {
		//super paint function :)  but it also lets us use the default paint function from swing
		super.paint(g);
		//I DONT UNDERSTAND WHAT THIS IS DOING
		for (int i = 3; i <= 7; i++) {
			if ((x1 >= margin) && (x2 <= getWidth() - margin) && (y1 >= margin) && (y2 <= getHeight() - margin)) g.drawLine(x1 + i, y1 + i, x2 + i, y2 + i);
		}
		//set the background to dark grey
		g.setColor(Color.DARK_GRAY);
		//draw the dots in the given area with the given size
		for (int x = margin; x < getWidth(); x += cellSize()) {//bounds for x axis
			for (int y = margin; y < getHeight(); y += cellSize()) {//bounds for y axis
				g.fillOval(x, y, 10, 10);//draw dot 
			}
		}
		//if the cell is marked check for an owner and draw an initial in it because it is full
		for (int i = 0; i < markedCells.size(); i++) {//loop through all the marked cells
			if (markedCells.get(i).owner == 1) {//if it has an owner, draw initial
				g.drawString(String.valueOf(player1.charAt(0)), markedCells.get(i).westCoord + 8, markedCells.get(i).northCoord + 20);
			} else {
				g.drawString(String.valueOf(player2.charAt(0)), markedCells.get(i).westCoord + 8, markedCells.get(i).northCoord + 20);
			}
		}
		
		for (int i = 1; i < cellArray.size(); i++) {
			for (int j = 3; j <= 7; j++) {
				if (cellArray.get(i).north) g.drawLine(cellArray.get(i).westCoord + j, cellArray.get(i).northCoord + j, cellArray.get(i).eastCoord + j, cellArray.get(i).northCoord + j);
				if (cellArray.get(i).south) g.drawLine(cellArray.get(i).westCoord + j, cellArray.get(i).southCoord + j, cellArray.get(i).eastCoord + j, cellArray.get(i).southCoord + j);
				if (cellArray.get(i).east) g.drawLine(cellArray.get(i).eastCoord + j, cellArray.get(i).northCoord + j, cellArray.get(i).eastCoord + j, cellArray.get(i).southCoord + j);
				if (cellArray.get(i).west) g.drawLine(cellArray.get(i).westCoord + j, cellArray.get(i).northCoord + j, cellArray.get(i).westCoord + j, cellArray.get(i).southCoord + j);
			}
		}
		
	}
	
	public void highlightLine(int a, int b, int c, int d) {
		x1 = a;
		y1 = b;
		x2 = c;
		y2 = d;
	}
	
	public void markCell(Cell cellToMark, String a, String b) {
		markedCells.push(cellToMark);
		player1 = a;
		player2 = b;
		
	}
	
	public void initializeLL() {
		for (int i = 0; i < (cells) * (cells) + 1; i++) {
			Cell cell = new Cell();
			cell.index = i;
			cell = getCoordinates(cell);
			cellArray.add(i, cell);
		}
	

		
	}
	
	public Cell getCoordinates(Cell cell) {
		if (cell.index != 0) {
			cell.northCoord = margin + ((cell.index - 1) / cells) * cellSize();
			cell.southCoord = cell.northCoord + cellSize();
			
			cell.westCoord = margin + (((cell.index - 1) % cells) * cellSize());
			cell.eastCoord = cell.westCoord + cellSize();
		}
		return cell;
	}
	
	public int cellSize() {
		return((getWidth() - cells - (2 * margin)) / cells);
	}
	
	public Cell determineCurrentCell(int x, int y, boolean highlight) {
		
		int i = 0, j = 1;
		int northCoord, southCoord, westCoord, eastCoord, index = 0;
		Cell currentCell;
		
		while(true) {
			if (((margin + (cellSize() * i)) < x) && (x < (margin + (cellSize() * j)))) {
				westCoord = (margin + (cellSize() * i));
				eastCoord = (margin + (cellSize() * j));
				index = j;
				break;
			} else {
				i++;
				j++;
			}
		}
		i = 0;
		j = 1;
		while(true) {
			if (((margin + (cellSize() * i))< y) && (y < (margin + (cellSize() * j)))) {
				northCoord = (margin + (cellSize() * i));
				southCoord = (margin + (cellSize() * j));
				index += i * (cells);
				break;
			} else {
				i++;
				j++;
			}
		}
		if ((index <= (cells) * (cells)) && (!highlight)) {
			currentCell = new Cell();
			currentCell.index = index;
			currentCell.northCoord = northCoord;
			currentCell.southCoord = southCoord;
			currentCell.eastCoord = eastCoord;
			currentCell.westCoord = westCoord;
			return currentCell;
		} else if ((index <= (cells) * (cells)) && (highlight)) {
			currentCell = new Cell();
			currentCell.index = index;
			currentCell.northCoord = northCoord;
			currentCell.southCoord = southCoord;
			currentCell.eastCoord = eastCoord;
			currentCell.westCoord = westCoord;
			return currentCell;
		}
		
		
		return null;
	}
	public void restartGame() {
		
		cellArray.clear();
		markedCells.clear();
		Dots.player1ScoreCount = 0;
		Dots.player2ScoreCount = 0;
		initializeLL();
	}

}
