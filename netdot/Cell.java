
/*
 * Names:
 * Nathan Spelts
 * Thomas Thanem
 * Logan McCartney
 * 
 * */

public class Cell {
	
	// Cell can store coordinates, index, player who completed it, and booleans for when a particular wall is marked
	Integer index, westCoord, eastCoord, northCoord, southCoord, owner;
	Boolean north, south, east, west;
	
	// constructor which initialize values when a cell is created
	public Cell() {
		north = south = east = west = false;
		westCoord = eastCoord = northCoord = southCoord = owner = 0;
	}
	
	// toString method for debugging
	public String toString() {
		return("Index: " + index + " North: " + north + " South: " + south + " East: " + east + " West: " + west + "\nnorthCoord: " + northCoord + " southCoord: " + southCoord + " eastCoord: " + eastCoord + " westCoord: " + westCoord + "\n");
	}
}
