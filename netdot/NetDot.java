import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Scanner;

import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;


/*
 * Names:
 * Nathan Spelts
 * Logan McCartney
 * Thomas Thanem
 * 
 * Date:
 * May 21st, 2021
 * 
 * Class:
 * CSE 223
 * 
 * Description:
 * This class controls the graphical user interface layout, it controls mouse listeners, button listeners, text fields etc.
 * It also contains much of the logic for determining where a mouse click is. It contains logic for cell completion, name storage,
 * score keeping etc. As well as logic for when a bad move is made or for which player's turn it is.
 * */

@SuppressWarnings("serial")
public class NetDot extends JFrame {

	// variable initialization
	private JPanel contentPane;
	public JTextField player1field;
	public JTextField player2field;
	public JTextField whoseTurn;
	
	ServerSocket ss;
	Socket sock;
	MyThread netThread;
	public MyPanel playGround;
	public static NetDot frame;
	public Integer xReceived, yReceived;
	public boolean server = false;
	boolean started = false;
	
	boolean player1turn = true; // used for determining whose turn it is
	boolean completedBox = false, completedAboveBox = false, completedBelowBox = false, completedLeftBox = false, completedRightBox = false; // used for keeping track of whether a cell is completed
	boolean badMove = true; // keeps track of when a player tries to make a forbidden move, gives them another turn
	boolean gameStarted = false; //becomes true when the start/connect button is pressed
	Cell aboveCell, belowCell, leftCell, rightCell; // used for tracking cells around the current cell
	// for tracking scores
	static int player1ScoreCount = 0; 
	static int player2ScoreCount = 0;

	public JTextField player1Score;
	public JTextField player2Score;
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private JTextField ipField;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new NetDot();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public NetDot() {
		setBackground(Color.DARK_GRAY);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1155, 818);
		contentPane = new JPanel();
		contentPane.setForeground(Color.WHITE);
		contentPane.setBackground(Color.DARK_GRAY);
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		playGround = new MyPanel();
		playGround.setBounds(376, 29, 740, 740);
		playGround.setForeground(Color.LIGHT_GRAY);
		playGround.setBackground(Color.GRAY);
		contentPane.add(playGround);
		
		JRadioButton serverRadioButton = new JRadioButton("Server");
		buttonGroup_1.add(serverRadioButton);
		serverRadioButton.setForeground(Color.WHITE);
		serverRadioButton.setBackground(Color.DARK_GRAY);
		serverRadioButton.setBounds(12, 133, 127, 23);
		contentPane.add(serverRadioButton);
		
		JRadioButton clientRadioButton = new JRadioButton("Client");
		buttonGroup_1.add(clientRadioButton);
		clientRadioButton.setForeground(Color.WHITE);
		clientRadioButton.setBackground(Color.DARK_GRAY);
		clientRadioButton.setBounds(174, 133, 127, 23);
		contentPane.add(clientRadioButton);
		
		// listener that is activated when the mouse moves within the playground
		playGround.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				//if statement to remove mouse movement errors before the game starts.
				if(gameStarted) {	
					mouseMoveLogic(playGround, e.getX(), e.getY());
				}
			}
		});
		
		// listener for tracking where the mouse is clicked
		playGround.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				//if statement to remove mouse click errors before the game starts.
				if(gameStarted) {
					if (serverRadioButton.isSelected() && player1turn) {
						mouseClickLogic(e.getX(), e.getY());
						String moveData = e.getX() + " " + e.getY();
						netThread.send(moveData);
					} else if (clientRadioButton.isSelected() && !player1turn) {
						mouseClickLogic(e.getX(), e.getY());
						String moveData = e.getX() + " " + e.getY();
						netThread.send(moveData);
					}
				}
			}
		});
		// playGround.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		
		JTextPane cellCount = new JTextPane();
		cellCount.setBackground(Color.DARK_GRAY);
		cellCount.setForeground(Color.WHITE);
		cellCount.setFont(new Font("Dialog", Font.PLAIN, 12));
		cellCount.setEditable(false);
		cellCount.setText(Integer.toString(playGround.cells + 1));
		cellCount.setBounds(469, 2, 21, 18);
		contentPane.add(cellCount);
		
		JButton zoomIn = new JButton("+");
		zoomIn.setBackground(Color.GRAY);
		zoomIn.setForeground(Color.LIGHT_GRAY);
		zoomIn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// flag prevents zooming once game has begun
				if (!playGround.initialized) {
					// adds one cell
					playGround.cells += 1;
					// changes cell text
					cellCount.setText(Integer.toString(playGround.cells + 1));
					// repaints grid
					repaint();
				}
			}
		});
		zoomIn.setBounds(12, 29, 117, 25);
		contentPane.add(zoomIn);
		
		JButton zoomOut = new JButton("-");
		zoomOut.setForeground(Color.LIGHT_GRAY);
		zoomOut.setBackground(Color.GRAY);
		zoomOut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// flag prevents zooming once game has begun
				if (!playGround.initialized) {
					// removes one cell
					playGround.cells -= 1;
					// changes cell count text
					cellCount.setText(Integer.toString(playGround.cells + 1));
					// repaints grid
					repaint();
				}
			}
		});
		zoomOut.setBounds(174, 29, 117, 25);
		contentPane.add(zoomOut);
		
		JLabel lblCellCount = new JLabel("Cell Count:");
		lblCellCount.setForeground(Color.WHITE);
		lblCellCount.setBounds(382, 0, 93, 25);
		contentPane.add(lblCellCount);
		
		player1field = new JTextField();
		player1field.setText("Server");
		player1field.setEditable(false);
		player1field.setForeground(Color.BLACK);
		player1field.setBackground(Color.GRAY);
		player1field.setBounds(15, 106, 114, 19);
		contentPane.add(player1field);
		player1field.setColumns(10);
		
		player2field = new JTextField();
		player2field.setText("Client");
		player2field.setEditable(false);
		player2field.setForeground(Color.BLACK);
		player2field.setBackground(Color.GRAY);
		player2field.setBounds(174, 106, 114, 19);
		contentPane.add(player2field);
		player2field.setColumns(10);
		
		JLabel lblPlayer = new JLabel("Server");
		lblPlayer.setForeground(Color.LIGHT_GRAY);
		lblPlayer.setBounds(15, 79, 70, 15);
		contentPane.add(lblPlayer);
		
		JLabel lblPlayer_1 = new JLabel("Client");
		lblPlayer_1.setForeground(Color.LIGHT_GRAY);
		lblPlayer_1.setBounds(174, 79, 70, 15);
		contentPane.add(lblPlayer_1);
		
		JButton start = new JButton("Start");
		start.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!started) {
					if (player1field.getText().substring(0, 1).equals(player2field.getText().substring(0, 1))) {
						whoseTurn.setText("The names must have different initials.");
					} else {
						if (serverRadioButton.isSelected()) {
							serverRadioButton.setEnabled(false);
							try {
								ss = new ServerSocket(1234);
							}catch(Exception e) {
								System.out.println("bruh your serversocket didnt create bro" + e);
							}
						
							try {
								sock = ss.accept(); // receives the connection
							}catch(Exception e) {
								System.out.println("Error homie, your sock has not been accepted..." + e);
							}
							netThread = new MyThread(sock);
							netThread.start();
							gameStarted = true;//start the game
							server = true;
							netThread.send(player1field.getText());
						} else if (clientRadioButton.isSelected()) {
							clientRadioButton.setEnabled(false);
							ipField.setEnabled(false);
							try {
								sock = new Socket(ipField.getText(), 1234);
							} catch (Exception e) {
								System.out.println("Error: " + e);
							}
							netThread = new MyThread(sock);
							netThread.start();
							netThread.giveMePlayGround(playGround);
							gameStarted = true;//start the game
							try {
								netThread.send(player2field.getText());
							} catch (Exception e) {
								System.out.println("Error: " + e);
							}
						}
						
						// initializes LinkedList when start is pressed
						if (!playGround.initialized) playGround.initializeLL();
						// flag prevents zooming and name changes
						playGround.initialized = true;
	
						// player 1 goes first message
						whoseTurn.setText(player1field.getText() + " goes first.");
						
						started = true;
						start.setText("Quit");
					}
				} else {
					netThread.send("Q");
					quitGame();
				}
			}
		});
		
		//
		//Logan added code bruh
		//
		
		//changes name of start button to address the action it will be doing. (the client will connect to the server.) 
		clientRadioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				start.setText("Connect");
				//add in the connect button
				start.setBackground(Color.GRAY);
				start.setBounds(95, 200, 117, 25);
				contentPane.add(start);
				contentPane.repaint();
				player1field.setEditable(false);
				player2field.setEditable(true);
				contentPane.add(ipField);
				contentPane.repaint();
			}
		});
		//if the server is selected, disable the radiobuttons.
		serverRadioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				start.setText("Start");
				//add in the start button
				start.setBackground(Color.GRAY);
				start.setBounds(95, 177, 117, 25);
				contentPane.add(start);
				player1field.setEditable(true);
				player2field.setEditable(false);
				contentPane.remove(ipField);
				contentPane.repaint();
			}
		});
		
		//
		//End of Logans code bruh
		//
		
		//I commented this out to make sure the player doesnt start the game before selecting server/client first.
	//	start.setBackground(Color.GRAY);
	//	start.setBounds(95, 177, 117, 25);
	//	contentPane.add(start);
		
		whoseTurn = new JTextField();
		whoseTurn.setBorder(null);
		whoseTurn.setForeground(Color.LIGHT_GRAY);
		whoseTurn.setBackground(Color.DARK_GRAY);
		whoseTurn.setEditable(false);
		whoseTurn.setBounds(12, 243, 279, 35);
		contentPane.add(whoseTurn);
		whoseTurn.setColumns(10);
		
		player1Score = new JTextField();
		player1Score.setText("Player 1's Score: ");
		player1Score.setBorder(null);
		player1Score.setEditable(false);
		player1Score.setForeground(Color.LIGHT_GRAY);
		player1Score.setBackground(Color.DARK_GRAY);
		player1Score.setBounds(15, 320, 137, 19);
		contentPane.add(player1Score);
		player1Score.setColumns(10);
		
		player2Score = new JTextField();
		player2Score.setBorder(null);
		player2Score.setText("Player 2's Score:");
		player2Score.setEditable(false);
		player2Score.setForeground(Color.LIGHT_GRAY);
		player2Score.setBackground(Color.DARK_GRAY);
		player2Score.setBounds(15, 368, 137, 19);
		contentPane.add(player2Score);
		player2Score.setColumns(10);
		
		ipField = new JTextField();
		ipField.setForeground(Color.BLACK);
		ipField.setBackground(Color.GRAY);
		ipField.setText("localhost");
		ipField.setBounds(174, 164, 117, 19);
		ipField.setColumns(10);
		
	}
	private String getDirection(Cell currentCell, int xCoord, int yCoord) {
		boolean north = false, south = false, east = false, west = false;
		int westDist = xCoord - currentCell.westCoord;
		int eastDist = currentCell.eastCoord - xCoord;
		int northDist = yCoord - currentCell.northCoord;
		int southDist = currentCell.southCoord - yCoord;
		
		//System.out.println(currentCell);
		
		// here we see if we're closer to the east or west
		if (westDist < eastDist) west = true;
		else east = true;
		
		// here we see if we're farther south or north
		if (northDist < southDist) north = true;
		else south = true;
		
		if (west && north) {
			// since we're close to north and west
			// we see which one we're closer to
			if (westDist < northDist) return("w");
			else return("n");
		} else if (west && south) {
			// since we're close to south and west
			// we see which one we're closer to
			if (westDist < southDist) return("w");
			else return("s");
		} else if (east && north) {
			// since we're close to north and east
			// we see which one we're closer to
			if (eastDist < northDist) return("e");
			else return("n");
		} else if (east && south) {
			// since we're close to east and south
			// we see which one we're closer to
			if (eastDist < southDist) return("e");
			else return("s");
		}
		return("");
	}
	
	private void badMove() {
		badMove = true;
		if (player1turn) whoseTurn.setText("Bad move. " + "It's still " + player1field.getText() + "'s turn.");
		else whoseTurn.setText("Bad move. " + "It's still " + player2field.getText() + "'s turn.");
	}
	
	private void storeDataAndCheckForCompletion(String direction, Cell currentCell, MyPanel playGround) {
		// badMove is set to false, so the player doesn't get another turn
		badMove = false;
		
		// the north is set to true for the currentCell, and it's updated with any information already present at that index
		// within the linked list.
		currentCell.north = playGround.cellArray.get(currentCell.index).north;
		currentCell.south = playGround.cellArray.get(currentCell.index).south;
		currentCell.east = playGround.cellArray.get(currentCell.index).east;
		currentCell.west = playGround.cellArray.get(currentCell.index).west;
		
		if (direction.equals("n")) {
			currentCell.north = true;
			// this if statement prevents leaving the bounds of the index
			if (currentCell.index - playGround.cells >= 0) {
				// here we retrieve the cell above so we can mark it's south variable as true
				aboveCell = playGround.cellArray.get(currentCell.index - playGround.cells);
				aboveCell.south = true;
				
				// this is where we check if the above cell was completed with our move
				if (aboveCell.north && aboveCell.east && aboveCell.west) {
					// if it was, we mark this variable and deal with it later
					completedAboveBox = true;
				}
			}
		}
		else if (direction.equals("s")) { 
			currentCell.south = true;
			// if statement catches edge case to prevent out of bounds exception
			if (!(playGround.cells + currentCell.index - 1 >= playGround.cells * playGround.cells)) {
				// grabs the location of cell below current cell
				belowCell = playGround.cellArray.get(playGround.cells + currentCell.index);
				
				// sets the north field of the field below to true
				belowCell.north = true;
				// checks to see if the box below became complete
				if (belowCell.south && belowCell.east && belowCell.west) {
					completedBelowBox = true;
				}
			}
		}
		else if (direction.equals("e")) {
			currentCell.east = true;
			// if statement prevents out of bounds exception
			if (currentCell.index % playGround.cells != 0) {
				// sets rightCell equal to the cell to the right of the current cell
				rightCell = playGround.cellArray.get(currentCell.index + 1);
				
				// sets right cell's west field to true
				rightCell.west = true;
				// checks if the rightCell was completed
				if (rightCell.north && rightCell.east && rightCell.south) {
					completedRightBox = true;
				}
			}
		}
		else if (direction.equals("w")) {
			currentCell.west = true;
			// if statement makes sure we don't go out of bounds
			if ((currentCell.index % playGround.cells) - 1 != 0) {
				// sets left cell equal to cell to the left of current cell
				leftCell = playGround.cellArray.get(currentCell.index - 1);
				// sets east flag of left cell to true
				leftCell.east = true;
				// checks to see if we completed the cell to the left
				if (leftCell.north && leftCell.west && leftCell.south) {
					completedLeftBox = true;
				}
			}
		}
		
		// now we add the currentCell to the link list (which tells the paint method to draw it
		playGround.cellArray.set(currentCell.index, currentCell);
		
		// checks if box is completed and sets flag if it is
		if (currentCell.north && currentCell.south && currentCell.east && currentCell.west) completedBox = true;
	}
	
	private void cellCompleted(Cell cell, MyPanel playGround) {
		// if player1 completed it
		if (player1turn) {
			// sets owner flag to one
			cell.owner = 1;
			// tell markCell method so it can put initials on cell
			playGround.markCell(cell, player1field.getText(), player2field.getText());
			// add 1 to player 1's score
			player1ScoreCount++;
			// update player 1's score text display
			player1Score.setText(player1field.getText() + "'s Score: " + player1ScoreCount);
		// if player 2 completed it
		} else {
			// sets owner flag to 2
			cell.owner = 2;
			// increment player 2's score
			player2ScoreCount++;
			// tell markCell method to write initials in cell
			playGround.markCell(cell, player1field.getText(), player2field.getText());
			// update score text
			player2Score.setText(player2field.getText() + "'s Score: " + player2ScoreCount);
		}
	}
	
	public void mouseClickLogic(int xCoord, int yCoord) {
		// this if statement prevents altering cells until the game has started
		if (playGround.initialized) {
			// this if statement is to catch the edge case where the margin is clicked and weird behavior occurs
			if (!(xCoord >= playGround.getWidth() - playGround.margin - 6)) {
				String direction = "";
				// this variable calls a method in MyPanel to determine the index and corner coordinates for our cell
				Cell currentCell = playGround.determineCurrentCell(xCoord, yCoord, false);
				// currentCell is null if the user clicks in the margin.
				if (currentCell != null) {
					
					direction = getDirection(currentCell, xCoord, yCoord);
					
					// if we determines we're closest to the north we go here
					if (direction.equals("n")) {
						if (playGround.cellArray.get(currentCell.index).north)  badMove();
						else storeDataAndCheckForCompletion(direction, currentCell, playGround);
					// if we're closest to south we go here
					} else if (direction.equals("s")) {
						if (playGround.cellArray.get(currentCell.index).south) badMove();
						else storeDataAndCheckForCompletion(direction, currentCell, playGround);
					// if we're closest to east, go here
					} else if (direction.equals("e")) {
						if (playGround.cellArray.get(currentCell.index).east)  badMove();
						else storeDataAndCheckForCompletion(direction, currentCell, playGround);
					// if the closest edge was west, go here
					} else if (direction.contentEquals("w")) {
						if (playGround.cellArray.get(currentCell.index).west)  badMove();
						else storeDataAndCheckForCompletion(direction, currentCell, playGround);
					}
					// skips this logic if badMove is true
					if (!badMove) {
						// if statement triggered if any box is completed
						if ((completedBox) || (completedAboveBox) || (completedBelowBox) || (completedLeftBox) || (completedRightBox)) {
							// if current cell was completed
							if (completedBox) {
								cellCompleted(currentCell, playGround);
								// turn off completed box flag
								completedBox = false;
							}
							// if cell above was completed
							if (completedAboveBox) {
								cellCompleted(aboveCell, playGround);
								// turn off completed box flag
								completedAboveBox = false;
							}
							// if cell below was completed
							if (completedBelowBox) {
								cellCompleted(belowCell, playGround);
								// turn off completed box flag
								completedBelowBox = false;
							}
							// if cell to the left was completed
							if (completedLeftBox) {
								cellCompleted(leftCell, playGround);
								// turn off completed box flag
								completedLeftBox = false;
							}
							// if cell to the right is completed
							if (completedRightBox) {
								cellCompleted(rightCell, playGround);
								// turn off completed box flag
								completedRightBox = false;
							}
						// switches turns
						} else {
							if (player1turn) {
								player1turn = false;
							} else {
								player1turn = true;
							}
						}
					}
				}
				// changes turn message in text field
				if (!badMove) {
					if (player1turn) {
						whoseTurn.setText("It's " + player1field.getText() + "'s turn.");
					} else {
						whoseTurn.setText("It's " + player2field.getText() + "'s turn.");
					}
				}
				// repaints everything that was changed
				repaint();

				if (player1ScoreCount + player2ScoreCount == playGround.cells * playGround.cells) {
					if (player1ScoreCount > player2ScoreCount) {
						whoseTurn.setText(player1field.getText() + " won!");
					} else {
						whoseTurn.setText(player2field.getText() + " won!");
					}
				}
				return;
			}
		}
	}
	
	private void mouseMoveLogic(MyPanel playGround, int xCoord, int yCoord) {
		// variable that tracks the current cell the mouse is moving in
		Cell currentCell;
		String direction = "";
		// this is where we determine the coordinates of the corners of the current cell, as well as the index
		currentCell = playGround.determineCurrentCell(xCoord, yCoord, true);
		// if we're in the margin, currentCell will be null.
		if (currentCell != null) {
			
			direction = getDirection(currentCell, xCoord, yCoord);
			
			// now we tell MyPanel to highlight the line we're closest to for a cool visual effect!
			if (direction.equals("n")) playGround.highlightLine(currentCell.westCoord, currentCell.northCoord, currentCell.eastCoord, currentCell.northCoord);
			else if (direction.equals("s")) playGround.highlightLine(currentCell.westCoord, currentCell.southCoord, currentCell.eastCoord, currentCell.southCoord);
			else if (direction.equals("e")) playGround.highlightLine(currentCell.eastCoord, currentCell.northCoord, currentCell.eastCoord, currentCell.southCoord);
			else if (direction.contentEquals("w")) playGround.highlightLine(currentCell.westCoord, currentCell.northCoord, currentCell.westCoord, currentCell.southCoord);
			// we tell MyPanel to repaint with it's new data
			repaint();
		}
	}
	
	public static NetDot giveMeDaFrame() {
		return frame;
	}
	
	public void quitGame() {
		try {
			sock.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
	}
}
