/*
Name: Nathan Spelts
Date: April 13th, 2021
Class: CSE 223
Description: This class creates a data type for fractions. It allows you to create fractions,
as well as do elementary operations on them such as addition, subtraction, division and multiplication.
The class returns NaN when working with numbers divided by zero, as well as any numbers affected by
these undefined numbers. There's also methods for converting to double, finding the GCD, reducing fractions,
and for getting the denomenator or numerator of the fraction. The class handles conversion to a string for
fractions as well.
*/

class Fraction {
	// declare our variables
	int numerator, denomenator;

	// defining the constructor for a single argument
	public Fraction(int wholeNum) {
		// assigns numerator to provided number, sets denomenator to 1 (for whole number)
		numerator = wholeNum;
		denomenator = 1;
    	}
    	// definining the constructor for two arguments
    	public Fraction(int newNumerator, int newDenomenator) {
        
		// variable to store greatest common denomenator
		int gcd;

		// checks for division by zero
        	if (newDenomenator == 0) {
			// we don't want to reduce if number is undefined (because of div by 0 error)
			numerator = newNumerator;
			denomenator = newDenomenator;
		} else {
			// gets GCD of num and denom
			gcd = GCD(newNumerator, newDenomenator);

        		// assign numerator and denomenator based on parameters after reducing them
        		numerator = newNumerator / gcd;
        		denomenator = newDenomenator / gcd;
		}
	}

	// Method that adds fractions together by converting them to have a common denomenator,
	// then adding the numerators together and reducing.
	public Fraction add(Fraction n) {
		// initialize temperary variables
        	Fraction result = new Fraction(0);
        	Fraction a = new Fraction(numerator, denomenator);
        	Fraction b = new Fraction(n.numerator, n.denomenator);
		// if the fractions don't share a common denomenator
        	if (a.denomenator != b.denomenator) {

            		// multiply a's and b's numerator by the denomenator of the other.
            		a.numerator = b.denomenator * a.numerator;
            		b.numerator = a.denomenator * b.numerator;
			// multiply a's denomenator by the denomenator of the other.
			        a.denomenator = b.denomenator * a.denomenator; 
        	}
        	// adds a's numerator to b's numerator
        	result.numerator = a.numerator + b.numerator;
        	// assign new denomenator
        	result.denomenator = a.denomenator;
        	return(result.reduce());
    	}
    
    	// Method that subtracts fractions by converting them to have a common denomenator,
	// then subtracting the numerators and reducing.
    	public Fraction sub(Fraction n) {
        	Fraction result = new Fraction(0);
        	Fraction a = new Fraction(numerator, denomenator);
        	Fraction b = new Fraction(n.numerator, n.denomenator);

        	// if the fractions don't share a common denomenator
        	if (a.denomenator != b.denomenator) {
            		// multiply a's and b's numerator by the denomenator of the other.
            		a.numerator = b.denomenator * a.numerator;
			b.numerator = a.denomenator * b.numerator;
            		// multiply a's and b's denomenator by the denomenator of the other.
			a.denomenator = b.denomenator * a.denomenator; 
            		b.denomenator = a.denomenator * b.denomenator;	
        	}
        	// subtract b's numerator from a's numerator
        	result.numerator = a.numerator - b.numerator;
        	// assign new denomenator
        	result.denomenator = a.denomenator;
        	return(result.reduce());
    	}

    	// Method that multiplies fractions simply by multiplying the numerators, multiplying
	// the denomenators, then reducing.
    	public Fraction mul(Fraction n) {
		// initialize temperary variables
        	Fraction result = new Fraction(0);
        	Fraction a = new Fraction(numerator, denomenator);
        	Fraction b = new Fraction(n.numerator, n.denomenator);

        	// multiplies numerator by numerator and denomenator by denomenator
        	result.numerator = a.numerator * b.numerator;
        	result.denomenator = a.denomenator * b.denomenator;
		// returns reduced result
        	return(result.reduce());
    	}
    
    	// Method that divides fractions by taking the reciprocal of one fraction and
	// multiplying it by the other, then reducing.
    	public Fraction div(Fraction n) {
		// initialize temperary variables
        	Fraction result = new Fraction(0);
        	Fraction a = new Fraction(numerator, denomenator);
        	Fraction b = new Fraction(n.numerator, n.denomenator);

        	// multiplies a by the reciprocal of b. Which is the same as dividing
        	result.numerator = a.numerator * b.denomenator;
        	result.denomenator = a.denomenator * b.numerator;
		// checks for a 0 denomenator before reducing
        	if (result.denomenator == 0) {
			// does not reduce if potential for division by 0
			return(result);
		} else {
			// returns reduced result
			return(result.reduce());
		}
    	}

	// Method that converts fractions to double precision floating point numbers by
	// casting the numerator to a double and dividing by a denomenator casted to a double.
	public double toDouble() {
		// in case of division by zero
		if (denomenator == 0) {
			// return NaN
			return(Double.NaN);
		}
		// cast numerator and denomenator to doubles and divide numerator by denomenator
		double quotient = (double)numerator / (double)denomenator;
		// return quotient
		return(quotient);
	}

	// Method for getting numerator
	public int getNum() {
		return(numerator);
	}

	// Method for getting denomenator
	public int getDenom() {
		return(denomenator);
	}

	// Method that finds the greatest common denomenator by taking in a
	// numerator and denomenator and running euclids algorithm on them.
	private int GCD(int num, int denom) {
		// initialize temperary variables
		int a = num;
		int b = denom;
		int c;
		while (true) {
			// take the modulo of a by b
			c = a % b;
			// if it's 0 then break, we have our GCD
			if (c == 0) {
				break;
			} else {
				// else swap variables and loop
				a = b;
				b = c;
			}
		}
		// return gcd
		return b;
	}

	// Method that reduces fractions by dividing the numerator and denomenator
	// by a greatest common denomenator.
	private Fraction reduce() {
		// initialize return object and gcd variable
		Fraction result = new Fraction(0);
		int gcd;
		// gets GCD
		gcd = GCD(numerator, denomenator);
		// divide numerator and denomenator by newfound GCD
		result.numerator = numerator / gcd;
		result.denomenator = denomenator / gcd;
		return(result);
	}	

    	// Method that tells JVM how to print fractions in a visually accurate
	// and appealing way.
    	public String toString() {
		// printing rules for undefined number
		if (denomenator == 0) {
			return("NaN");
		}
		// if numerator and denomenator are negative
		if (numerator < 0 && denomenator < 0) {
			// make num and denom non-negative
			numerator *= -1;
			denomenator *= -1;
		// if only denomenator is negative
		} else if (numerator > 0 && denomenator < 0) {
			// make num negative and denom non-negative
			numerator *= -1;
			denomenator *= -1;
		}
		// printing rules for rational number
        	if (denomenator != 1) {
            		return(numerator + "/" + denomenator);
		// printing rules for whole number
        	} else {
            		return(numerator + "");
        	}
    	}
}
